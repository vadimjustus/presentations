# DevOps 
## in Web-Agenturen

-- next slide --

## Vorstellung

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/vadim.jpg" alt="Vadim Justus" style="width: 150px;"/>

## Vadim Justus
* <!-- .element: class="fragment" data-fragment-index="1" --> TechDivision
* <!-- .element: class="fragment" data-fragment-index="2" --> IC DevOps
* <!-- .element: class="fragment" data-fragment-index="3" --> Head of Magento

-- next page --

## Wer seid ihr?

-- next slide --

## Was ist DevOps?

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/devops-dudes-climbing.gif" alt="Zusammenarbeit" style="width: 80%;"/>

Development + Operations

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/devops_boat.png" alt="Kommunikation und Philosophie" style="width: 80%;"/>

Kommunikation und Philosophie

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/devops_tools.png" alt="Tools und Werkzeuge" style="width: 80%;"/>

Tools

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/devops_jobs.png" alt="Jobs" style="width: 80%;"/>

Job-Titel

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/devops_books.png" alt="Books" style="width: 62%;"/>

Hype / Trend => Geschäft

-- next slide --

## Warum DevOps?

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/software_future.jpg" alt="Software in der Zukunft" style="width: 80%;"/>

Neue Software - Neue Hardware

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/virtualisierung.jpg" alt="Veränderung der Berufsfelder" style="width: 80%;"/>

Veränderung der Berufsfelder

-- next slide --

## DevOps - Success-Stories

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/logos/amazon.jpg" alt="Amazon" style="width: 30%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/google.jpg" alt="Google" style="width: 33%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/spotify.jpg" alt="Spotify" style="width: 15.2%;"/>

<img src="presentations/DevOps-im-Agenturumfeld/images/logos/etsy.jpg" alt="etsy.com" style="width: 22%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/wooga.png" alt="wooga" style="width: 30%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/facebook.png" alt="facebook" style="width: 30%;"/>

<img src="presentations/DevOps-im-Agenturumfeld/images/logos/netflix.png" alt="Netflix" style="width: 22%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/twitter.jpg" alt="twitter" style="width: 20%; padding: 10px; background: #fff;"/>

-- next page --

## Gemeinsamkeit?

* <!-- .element: class="fragment" data-fragment-index="1" --> Erfolg
* <!-- .element: class="fragment" data-fragment-index="2" --> Internet
* <!-- .element: class="fragment" data-fragment-index="3" --> Produkt

-- next slide --

## Und Agenturen?

* <!-- .element: class="fragment" data-fragment-index="1" --> ~~Produkt~~ / Dienstleister
* <!-- .element: class="fragment" data-fragment-index="2" --> ~~Betrieb (IT)~~
* <!-- .element: class="fragment" data-fragment-index="3" --> verschiedene Kunden / Projekte

-- next page --

<img src="presentations/DevOps-im-Agenturumfeld/images/logos/amazon.jpg" alt="Amazon" style="width: 30%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/google.jpg" alt="Google" style="width: 33%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/spotify.jpg" alt="Spotify" style="width: 15.2%;"/>

<img src="presentations/DevOps-im-Agenturumfeld/images/logos/etsy.jpg" alt="etsy.com" style="width: 22%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/wooga.png" alt="wooga" style="width: 30%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/facebook.png" alt="facebook" style="width: 30%;"/>

<img src="presentations/DevOps-im-Agenturumfeld/images/logos/netflix.png" alt="Netflix" style="width: 22%;"/>
<img src="presentations/DevOps-im-Agenturumfeld/images/logos/twitter.jpg" alt="twitter" style="width: 20%; padding: 10px; background: #fff;"/>

-- next page --

Ist DevOps in einer Agentur 
## nötig?

-- next page --

Ist DevOps in einer Agentur 
## möglich?

-- next page --

Problem 1:
## verschiedene Kunden / Projekte

<!-- .element: class="fragment" data-fragment-index="1" --> **Jesper Richter-Reichhelm (Wooga):**

> <!-- .element: class="fragment" data-fragment-index="1" --> Was wäre, wenn ihr die Möglichkeit hättet eure Software nochmal zu entwickeln?

-- next page --

Problem 2:
## ~~Produkt~~ / Dienstleister

* <!-- .element: class="fragment" data-fragment-index="1" --> Releasemanagement
* <!-- .element: class="fragment" data-fragment-index="2" --> Continuous Delivery
* <!-- .element: class="fragment" data-fragment-index="3" --> Web-Bude > Software-House

-- next page --

Problem 3:
## ~~Betrieb (IT)~~

<table>
    <tr>
        <td>test</td>
        <td>test</td>
    </tr>
</table>

-- next page --

## Wie macht ihr DevOps?

-- next slide --

## Vielen Dank

Vadim Justus

v.justus@techdivision.com

