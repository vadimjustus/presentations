<?php

/**
 * @param array $data
 * @param string $template
 * @param string $prefix
 * @return string
 */
function applyDataToTemplate(array $data, $template, $prefix = null) {
    foreach ($data as $key => $value) {
        $varName = array();
        if (!empty($prefix)) {
            $varName[] = $prefix;
        }
        $varName[] = $key;
        $varName = implode('.', $varName);

        if (is_array($value)) {
            $template = applyDataToTemplate($value, $template, $varName);
        } else {
            $template = str_replace('{{ ' . $varName . ' }}', $value, $template);
        }
    }
    return $template;
}

if (!isset($_GET['presentation'])) {
    $directoryIteration = new DirectoryIterator(__DIR__ . '/presentations');

    $presentations = array();
    /** @var DirectoryIterator $iteration */
    foreach ($directoryIteration as $iteration) {
        if ($iteration->isDot() || $iteration->isFile()) {
            continue;
        }
        $data = json_decode(file_get_contents($iteration->getPath() . '/' . $iteration->getBasename() . '/info.json'), true);
        $presentations[] = sprintf(
            '<a href="%s/">%s</a>',
            '?presentation=' . $iteration->getFilename(),
            $data['title']
        );
    }
    echo implode('<br />', $presentations);
    return;
}

$presentation = $_GET['presentation'];
$infoFile = __DIR__ . '/presentations/' . $presentation . '/info.json';

$data = array('name' => $presentation);
$data = array_merge($data, json_decode(file_get_contents($infoFile), true));

$template = file_get_contents(__DIR__ . '/templates/presentation.html');
echo applyDataToTemplate($data, $template);